package com.uhack.td.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.uhack.td.TDGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "UHack Tower Defense";
		config.width = 1200;
		config.height = 1200;
		new LwjglApplication(new TDGame(), config);
	}
}
