package com.uhack.td;


import java.util.List;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class TDGame implements ApplicationListener {

	private Stage stage;
	private TileMap tileMap;
	private Towers towers;
	private Wave wave1;
	private Path path;
	private CollisionDetector colDet;

	
	@Override
	public void create () {
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		
		int[][] mapGrid = {
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
				{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1},
				{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
				{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
				{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		Level level = new Level(mapGrid.length, mapGrid[0].length);
		level.fromMatrix(mapGrid);
		
		LevelToTileConverter converter = new LevelToTileConverter(level);
		tileMap = new TileMap(converter.convert(stage.getWidth(), stage.getHeight()));
		tileMap.setWidth(stage.getWidth());
		tileMap.setHeight(stage.getHeight());
		stage.addActor(tileMap);
		
		LevelToPathConverter pConverter = new LevelToPathConverter(level);
		path = new Path(pConverter.convert(stage.getWidth(), stage.getHeight())); 
		
		wave1 = new Wave(level);
		wave1.setPath(path);
		
		wave1.setWidth(stage.getWidth());
		wave1.setHeight(stage.getHeight());
		wave1.addActor(new Enemy(1, 480, 200));
		wave1.addActor(new Enemy(1, 400, 200));
		wave1.addActor(new Enemy(1, 450, 200));
		wave1.addActor(new Enemy(1, 450, 200));
		wave1.addActor(new Enemy(1, 450, 200));
		wave1.addActor(new Enemy(1, 450, 200));
		
		stage.addActor(wave1);
		
		towers = new Towers();
		towers.setWidth(stage.getWidth());
		towers.setHeight(stage.getHeight());
		towers.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (button == Input.Buttons.LEFT) {
					towers.addActor(new Tower(x - Tower.defaultWidth/2, y - Tower.defaultHeight/2));
					return true;
				}
				return false;
			}
		});
		stage.addActor(towers);
		colDet = new CollisionDetector(towers, wave1);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		float deltaTime = Gdx.graphics.getDeltaTime();
		wave1.update(deltaTime);
		
		List<Collision> cols = colDet.getCollisions();
		for (Collision col : cols) {
			//col.getA().setColor(new Color(Color.rgb888((float)Math.random(), (float)Math.random(), (float)Math.random())));
		}
		
		
		stage.act(deltaTime);
		stage.draw();
		
	}
	
	@Override
	public void dispose () {
		stage.dispose();
	}

	@Override
	public void resize(int width, int height) {
		float oldWidth = stage.getWidth();
		float oldHeight = stage.getHeight();
		
		stage.getViewport().update(width, height, true);
		/*for (Actor child : stage.getActors()) {
			child.setX(child.getX()/oldWidth*width);
			child.setY(child.getY()/oldHeight*height);
			child.setWidth(child.getWidth()/oldWidth*width);
			child.setHeight(child.getHeight()/oldHeight*height);
		}*/
		tileMap.resize(width, height);
		tileMap.setWidth(width);
		tileMap.setHeight(height);
		
		towers.resize(width, height, oldWidth, oldHeight);
		towers.setWidth(width);
		towers.setHeight(height);
		
		wave1.resize(width, height, oldWidth, oldHeight);
		wave1.setWidth(width);
		wave1.setHeight(height);
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
}
