package com.uhack.td;

import java.util.ArrayList;
import java.util.List;

public class Level {
	private List<List<Integer>> map;
	private int width;
	private int height;
	
	
	
	public Level(int width, int height) {
		this.width = width;
		this.height = height;
		this.map = new ArrayList<>(height);
		for (int i = 0; i < this.height; i++) {
			this.map.add(new ArrayList<>(this.width));
		}
	}

	public void fromMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				this.map.get(i).add(matrix[i][j]);
			}
		}
	}
	
	public Integer getTile(int x, int y) {
		return this.map.get(y).get(x);
	}
	
	public List<Integer> getRow(int y) {
		return this.map.get(y);
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}

}
