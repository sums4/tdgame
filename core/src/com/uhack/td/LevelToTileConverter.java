package com.uhack.td;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;

public class LevelToTileConverter extends LevelConverter<List<List<Tile>>> {
	private List<List<Tile>> tileMap;
	
	
	public LevelToTileConverter(Level lev) {
		super(lev);
		this.tileMap = new ArrayList<>(this.level.getHeight());
		for (int i = 0; i < this.level.getHeight(); i++) {
			this.tileMap.add(new ArrayList<>(this.level.getWidth()));
		}
	}

	@Override
	public List<List<Tile>> convert(float viewWidth, float viewHeight) {
		float tileWidth = viewWidth / this.level.getWidth();
		float tileHeight = viewHeight / this.level.getHeight();
		Color c = Color.GREEN;
		for (int i = 0; i < this.level.getHeight(); i++) {
			for (int j = 0; j < this.level.getWidth(); j++) {
				if (this.level.getTile(j, i) == 1) {
					c = Color.BLACK;
				} else {
					c = Color.GREEN;
				}
				Tile tile = new Tile(j, i, tileWidth, tileHeight, c);
				tile.setX(j*tileWidth);
				tile.setY(viewHeight - (i+1)*tileHeight);
				this.tileMap.get(i).add(tile);
			}
		}
		return this.tileMap;
	}

}
