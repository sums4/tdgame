package com.uhack.td;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class Collision {
	private Actor a;
	private Actor b;
	
	public Collision(Actor a, Actor b) {
		this.setA(a);
		this.setB(b);
	}

	public Actor getA() {
		return a;
	}

	public void setA(Actor a) {
		this.a = a;
	}

	public Actor getB() {
		return b;
	}

	public void setB(Actor b) {
		this.b = b;
	}
}
