package com.uhack.td;

public abstract class LevelConverter<T> {
	protected Level level;
	
	public LevelConverter(Level lev) {
		this.level = lev;
	}
	
	public abstract T convert(float viewWidth, float viewHeight);

}
