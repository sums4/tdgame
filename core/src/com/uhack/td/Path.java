package com.uhack.td;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.badlogic.gdx.math.Vector2;

public class Path {
	private List<Vector2> path;
	
	public Path(List<Vector2> p) {
		p.sort((Vector2 v1, Vector2 v2) -> Float.compare(v1.x, v2.x));
		this.path = new ArrayList<>();
		this.path.addAll(p);
		
	}
	
	public Vector2 getClosestPointToRightNotVisited(List<Vector2> visited, float x, float y) {
		List<Vector2> newPath = new ArrayList<>();
		newPath.addAll(this.path);
		newPath.removeAll(visited);
		newPath = newPath.stream().filter(v -> v.x >= x).collect(Collectors.toList());
		if (newPath.isEmpty()) {
			return null;
		} else {
			Collections.shuffle(newPath);
			return Collections.min(newPath, (Vector2 v1, Vector2 v2) -> {
				float d1 = v1.dst(x, y);
				float d2 = v2.dst(x, y);
				return Float.compare(d1, d2);
			});
		}
	}
}
