package com.uhack.td;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

public class Enemy extends Actor {
	public static float defaultRadius = 20;
	public static float defaultVel = 180;
	public static Color defaultColor = Color.RED;
	
	private float vel;
	
	private boolean isOnPath;
	
	private List<Vector2> visited;
	
	private ShapeRenderer renderer;
	
	public Enemy(float x, float y, float vel) {
		this.setX(x);
		this.setY(y);
		this.setWidth(Enemy.defaultRadius*2);
		this.setHeight(Enemy.defaultRadius*2);
		this.setColor(Enemy.defaultColor);
		this.vel = vel;
		this.isOnPath = false;
		renderer = new ShapeRenderer();
		setVisited(new ArrayList<>());
	}
	
	public void draw(Batch batch, float parentAlpha) {
		batch.end();
		
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		renderer.setTransformMatrix(batch.getTransformMatrix());
		renderer.translate(getX(), getY(), 0);
		
		renderer.begin(ShapeType.Filled);
		renderer.setColor(this.getColor());
		renderer.circle(0, 0, defaultRadius);
		renderer.end();
		
		batch.begin();
	}

	public float getVel() {
		return vel;
	}

	public Enemy setVel(float velX) {
		this.vel = vel;
		return this;
	}
	
	public boolean isOnPath() {
		return isOnPath;
	}

	public void setOnPath(boolean isOnPath) {
		this.isOnPath = isOnPath;
	}

	public List<Vector2> getVisited() {
		return visited;
	}

	public void setVisited(List<Vector2> visited) {
		this.visited = visited;
	}
}
