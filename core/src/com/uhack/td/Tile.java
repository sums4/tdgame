package com.uhack.td;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Tile extends Actor {
	private ShapeRenderer renderer;
	
	private int mapX;
	private int mapY;
	
	
	public Tile(int x, int y, float width, float height, Color color) {
		this.setColor(color);
		this.mapX = x;
		this.mapY = y;
		this.setWidth(width);
		this.setHeight(height);
		renderer = new ShapeRenderer();
	}
	
	public void draw(Batch batch, float parentAlpha) {
		batch.end();
		
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		renderer.setTransformMatrix(batch.getTransformMatrix());
		renderer.translate(getX(), getY(), 0);
		
		renderer.begin(ShapeType.Filled);
		renderer.setColor(this.getColor());
		renderer.rect(0, 0, getWidth(), getHeight());
		renderer.end();
		
		batch.begin();
	}
	
	public int getMapX() {
		return this.mapX;
	}
	
	public int getMapY() {
		return this.mapY;
	}

}
