package com.uhack.td;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class TileMap extends Group {
	private int mapHeight;
	private int mapWidth;
	
	public TileMap(List<List<Tile>> tiles) {
		for (int i = 0; i < tiles.size(); i++) {
			for (int j = 0; j < tiles.get(0).size(); j++) {
				Tile tile = tiles.get(i).get(j);
				this.addActor(tile);
			}
		}
		this.mapHeight = tiles.size();
		this.mapWidth = tiles.get(0).size();
	}
	
	public void resize(float viewWidth, float viewHeight) {
		Tile tile;
		float tileWidth = viewWidth / this.mapWidth;
		float tileHeight = viewHeight / this.mapHeight;
		for (Actor child : this.getChildren()) {
			tile = (Tile)child;
			tile.setWidth(tileWidth);
			tile.setHeight(tileHeight);
			tile.setX(tile.getMapX()*tileWidth);
			tile.setY(viewHeight - (tile.getMapY()+1)*tileHeight);
		}
	}
}
