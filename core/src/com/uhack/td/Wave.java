package com.uhack.td;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Wave extends Group {
	private Level level;
	private Path path;
	
	public Wave(Level lev) {
		this.level = lev;
	}
	
	public void setPath(Path path) {
		this.path = path;
	}
	
	public void update(float deltaTime) {
		float tileWidth = this.getWidth()/level.getWidth();
		//float tileHeight = this.getHeight()/level.getHeight();
		for (Actor child : this.getChildren()) {
			Enemy enemy = (Enemy)child;
			if (!enemy.hasActions()) {
				Vector2 point = this.path.getClosestPointToRightNotVisited(enemy.getVisited(), enemy.getX(), enemy.getY());
				if (point == null || this.level.getWidth()*tileWidth - enemy.getX() < tileWidth) {
					enemy.setColor(Color.GRAY);
				} else {
					enemy.addAction(Actions.moveTo(point.x, point.y, point.dst(enemy.getX(), enemy.getY()) / enemy.getVel() ));
					enemy.getVisited().add(point);
				}
			}
			
		}
	}
	
	public void resize(float viewWidth, float viewHeight, float oldViewWidth, float oldViewHeight) {
		for (Actor child : this.getChildren()) {
			child.setX(child.getX()/oldViewWidth*viewWidth);
			child.setY(child.getY()/oldViewHeight*viewHeight);
			child.setWidth(child.getWidth()/oldViewWidth*viewWidth);
			child.setHeight(child.getHeight()/oldViewHeight*viewHeight);
		}
	}

}
