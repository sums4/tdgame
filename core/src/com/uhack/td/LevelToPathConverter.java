package com.uhack.td;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class LevelToPathConverter extends LevelConverter<List<Vector2>> {
	private List<Vector2> path;
	
	
	public LevelToPathConverter(Level lev) {
		super(lev);
		this.path = new ArrayList<>();
	}

	@Override
	public List<Vector2> convert(float viewWidth, float viewHeight) {
		float tileWidth = viewWidth / this.level.getWidth();
		float tileHeight = viewHeight / this.level.getHeight();
		for (int i = 0; i < this.level.getHeight(); i++) {
			for (int j = 0; j < this.level.getWidth(); j++) {
				if (this.level.getTile(j, i) == 1) {
					Vector2 point = new Vector2();
					
					point.set(j*tileWidth + tileWidth/2.0f, viewHeight - (i+1)*tileHeight + tileHeight/2.0f);
					this.path.add(point);
				}
				
			}
		}
		return this.path;
	}

}
