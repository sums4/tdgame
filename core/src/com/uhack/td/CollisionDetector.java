package com.uhack.td;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class CollisionDetector {
	private Group sources;
	private Group targets;
	
	
	public CollisionDetector(Group sources, Group targets) {
		this.sources = sources;
		this.targets = targets;
	}
	
	public List<Collision> getCollisions() {
		List<Collision> collisions = new ArrayList<>();
		Rectangle tower = new Rectangle();
		Circle enemy = new Circle();
		for (Actor s : this.sources.getChildren()) {
			tower.set(s.getX(), s.getY(), s.getWidth(), s.getHeight());
			for (Actor t : this.targets.getChildren()) {
				enemy.set(t.getX(), t.getY(), t.getHeight()/2.0f);
				
				if (Intersector.overlaps(enemy, tower)) {
					collisions.add(new Collision(s, t));
				}
			}
		}
		
		return collisions;
	}
	

}
