package com.uhack.td;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class Towers extends Group {
	
	public Towers() {
		
	}
	
	public void resize(float viewWidth, float viewHeight, float oldViewWidth, float oldViewHeight) {
		for (Actor child : this.getChildren()) {
			child.setX(child.getX()/oldViewWidth*viewWidth);
			child.setY(child.getY()/oldViewHeight*viewHeight);
			child.setWidth(child.getWidth()/oldViewWidth*viewWidth);
			child.setHeight(child.getHeight()/oldViewHeight*viewHeight);
		}
	}
}
