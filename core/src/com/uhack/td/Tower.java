package com.uhack.td;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Tower extends Actor {
	
	public static float defaultWidth = 50;
	public static float defaultHeight = 50;
	public static Color defaultColor = Color.BLUE;
	
	private ShapeRenderer renderer;
	
	public Tower(float x, float y) {
		this.setX(x);
		this.setY(y);
		this.setWidth(Tower.defaultWidth);
		this.setHeight(Tower.defaultHeight);
		this.setColor(Tower.defaultColor);
		renderer = new ShapeRenderer();
	}
	
	public Tower(float x, float y, float width, float height) {
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
		this.setColor(Tower.defaultColor);
		renderer = new ShapeRenderer();
	}
	
	public Tower(float x, float y, float width, float height, Color color) {
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
		this.setColor(color);
		renderer = new ShapeRenderer();
	}
	
	public void draw(Batch batch, float parentAlpha) {
		batch.end();
		
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		renderer.setTransformMatrix(batch.getTransformMatrix());
		renderer.translate(getX(), getY(), 0);
		
		renderer.begin(ShapeType.Filled);
		renderer.setColor(this.getColor());
		renderer.rect(0, 0, getWidth(), getHeight());
		renderer.end();
		
		batch.begin();

	}

}
